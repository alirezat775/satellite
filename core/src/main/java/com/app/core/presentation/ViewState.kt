package com.app.core.presentation

sealed class ViewState<out T>
object ViewLoading : ViewState<Nothing>()
data class ViewData<T>(val data: T) : ViewState<T>()
data class ViewError(val message: String? = "") : ViewState<Nothing>()
data class ViewEmpty(val message: String? = "") : ViewState<Nothing>()
