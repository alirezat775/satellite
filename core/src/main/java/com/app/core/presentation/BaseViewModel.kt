package com.app.core.presentation

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.*

open class BaseViewModel : ViewModel() {

    fun launch(dispatcher: CoroutineDispatcher = Dispatchers.Main,block: suspend CoroutineScope.() -> Unit): Job {
        return viewModelScope.launch(context = dispatcher, block = block)
    }
}