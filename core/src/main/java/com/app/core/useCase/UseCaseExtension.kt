package com.app.core.useCase

import com.app.core.model.ErrorModel
import com.app.core.model.ResultModel

inline fun <R, T> ResultModel<T>.executeUseCase(
    ifSuccess: (value: T) -> R,
    ifError: (error: ErrorModel) -> R
): R {
    return when (this) {
        is ResultModel.Success<T> -> ifSuccess(value)
        is ResultModel.Error -> ifError(ErrorModel(error.message, error.type))
    }
}