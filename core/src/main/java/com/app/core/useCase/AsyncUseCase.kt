package com.app.core.useCase

interface AsyncUseCase<RQ, RS> {

    suspend fun executeAsync(rq: RQ): RS

}