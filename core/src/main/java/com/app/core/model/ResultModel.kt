package com.app.core.model

sealed class ResultModel<out V> {

    data class Success<V>(val value: V) : ResultModel<V>()

    data class Error(val error: ErrorModel) : ResultModel<Nothing>()
}

fun <T, R> ResultModel<T>.map(isSuccess: (T) -> R): ResultModel<R> {
    return when (this) {
        is ResultModel.Success -> ResultModel.Success(isSuccess.invoke(value))
        is ResultModel.Error -> ResultModel.Error(error)
    }
}

fun <T> ResultModel<T>.getOrNull(): T? {
    return if (this is ResultModel.Success) {
        this.value
    } else {
        null
    }
}

fun <T> ResultModel<T>.get(): T {
    return (this as ResultModel.Success).value
}

data class ErrorModel(
    val message: String? = null,
    val type: Exception? = null
)
