#Satellite App

## Architecture module

Generally app separated to 4 modules and 3 categories

![alt text](doc/modules.jpg "modules")

#### First category

App module handles bootstrap of application. For example, init dependency injection. The app module
has a self scope. Also, it depends on the core and uiKit module. The app module is responsible for
connecting modules. Handle application lifecycle. The app module has a single activity and attache
feature module in the container, such as a fragment of pure view. The app module has own application
service is used in the entire application like push notification.

#### Second category

##### Basic module (Core,UiKit)

UiKit module handle ui component of application. Every component is shared or not in presentation
layer. Implementation custom design system such as style, theme structure.

Core module handles fundamental components like an abstraction layer for useCase, view-state, or any
shared component in the whole of the application. Exclude UI component.

#### Third category

Feature module handles a special part of the application. For example satellite module responsible show
list of satellite and detail, every view logic, business logic, and data logic is related to this module.
the feature module has a self scope. Also, they depend on core, if does needed uiKit module. The
feature module doesn't any idea about the app module. The feature module handles the self lifecycle.
In the presentation layer, they just allow to use pure view or fragment, them doesn't know activity
state.

##### This is diagram to shows dependency between modules

![alt text](doc/dependencies.jpg "dependencies")

## Architecture layer

This architecture follow ups clean architecture principle, generally contains 4 parts
**Data**
Data package has three parts: Entity, Repository, and Data-source.

Entity: the data model for any data source like database entity,

Repository: responsible for any data fetch from or store to any data source, also map entity model
to domain model. Every method in repository class need implement with an interface

Data-source: the source of fetch and store data, Every method in repository class need implement
with an interface
**Domain**
Domain package has two parts: model and useCase

Model: the data model for any data fetch from or stored to the repository

UseCase: responsible for the single business logic of the app. Encapsulate and implement all of the
use cases of the system. They have no dependency and are totally isolated from things. Have two
abstract methods:
sync, aSync. Blocking task execute by sync method and non-blocking task execute by aSync method.

**Dependency injection**
DI package has two parts: module, injector Component: responsible for build modules in this graph.

Module: provide a class for injection in other classes. Every base packages have a self module for
providing class inside the package

Injector: build a dependency graph with modules

**Presentation**
Presentation package has two parts: view-model, view.

View: an instance of the fragment, responsible for any user action, and pass act to view-model.
Observe on view-model methods for changing UI with the state. State include :
ViewError, ViewEmpty, ViewData, ViewLoading,

View-model: responsible for executing a useCase task. Also, convert data for using view and emit
state. Inject useCases need per view-model

![alt text](doc/layer.png "layer")