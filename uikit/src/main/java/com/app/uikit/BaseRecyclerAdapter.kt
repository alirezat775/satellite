package com.app.uikit

import android.annotation.SuppressLint
import android.view.View
import androidx.recyclerview.widget.RecyclerView

abstract class BaseRecyclerAdapter :
    RecyclerView.Adapter<BaseRecyclerAdapter.BaseViewHolder<Any>>() {

    private var mItems: MutableList<Any> = mutableListOf()

    fun getItems(): MutableList<Any> {
        return mItems
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setItem(item: Any) {
        clearAll()
        this.mItems.add(item)
        notifyDataSetChanged()
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setItems(items: List<Any>) {
        clearAll()
        mItems.addAll(items)
        notifyDataSetChanged()
    }

    fun replaceItem(item: Any, position: Int) {
        if (mItems.isNullOrEmpty() && position <= mItems.lastIndex) {
            mItems[position] = item
            notifyItemRangeChanged(position, 1)
        }
    }

    fun addItem(item: Any) {
        mItems.add(item)
        val positionStart = mItems.size - 1
        notifyItemRangeChanged(positionStart, 1)
    }

    fun addItem(item: Any, position: Int) {
        mItems.add(position, item)
        notifyItemRangeChanged(position, 1)
    }

    fun addItems(items: List<Any>) {
        mItems.addAll(items)
        val positionStart = mItems.size - items.size
        notifyItemRangeChanged(positionStart, items.size)
    }

    fun addItemFirst(item: Any) {
        mItems.add(0, item)
        val positionStart = 0
        notifyItemRangeChanged(positionStart, 1)
    }

    fun addItemsFirst(items: List<Any>) {
        mItems.addAll(0, items)
        val positionStart = 0
        notifyItemRangeChanged(positionStart, 1)
    }

    fun removeItem(position: Int) {
        if (mItems.size > 0) {
            mItems.removeAt(position)
            notifyItemRemoved(position)
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    fun clearAll() {
        mItems.clear()
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: BaseViewHolder<Any>, position: Int) {
        holder.bind(mItems[position])
    }

    override fun getItemCount(): Int {
        return mItems.size
    }

    abstract class BaseViewHolder<out T>(view: View) : RecyclerView.ViewHolder(view) {
        abstract fun bind(data: @UnsafeVariance T)
    }

}
