package com.app.satelite

import android.app.Application
import com.app.satellite.di.SatelliteInjector
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class MainApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@MainApplication)
                .modules(SatelliteInjector.provideDependencies())
        }
    }
}