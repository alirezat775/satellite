package com.app.satellite

import com.app.satellite.domain.model.SatelliteDetailModel
import com.app.satellite.domain.model.SatelliteModel
import com.app.satellite.domain.model.SatellitePositionModel

object MockDataProvider {

    fun provideSatelliteList(): List<SatelliteModel> {
        return listOf(
            SatelliteModel(
                1,
                false,
                "satellite-first"
            ),
            SatelliteModel(
                2,
                true,
                "satellite-second"
            ),
            SatelliteModel(
                3,
                true,
                "satellite-third"
            ),
        )
    }

    fun provideSingleItemList(): List<SatelliteModel> {
        return listOf(
            SatelliteModel(
                1,
                false,
                "satellite-first"
            )
        )
    }

    fun provideSatelliteDetail(): SatelliteDetailModel {
        return SatelliteDetailModel(
            1,
            "satellite-a",
            124000,
            "21-12-2020",
            150,
            1000
        )
    }

    fun provideSatelliteEmptyDetail(): SatelliteDetailModel {
        return SatelliteDetailModel(
            0,
            "",
            0,
            "",
            0,
            0
        )
    }

    fun provideSatellitePositionDetail(): SatellitePositionModel {
        return SatellitePositionModel(
            0.41451242,
            0.51451242,
        )
    }

}