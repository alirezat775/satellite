package com.app.satellite.domain

import com.app.core.model.ResultModel
import com.app.core.useCase.executeUseCase
import com.app.satellite.MockDataProvider
import com.app.satellite.domain.useCase.GetSatelliteListUseCase
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.launch
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.TestCoroutineExceptionHandler
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@ExperimentalCoroutinesApi
@RunWith(JUnit4::class)
class GetSatelliteListUseCaseTest {

    private val repository: Repository = mockk()
    private val getSatelliteListUseCase = GetSatelliteListUseCase(repository)

    @Test
    fun `get empty list from repository`() {
        val testHandler = TestCoroutineExceptionHandler()
        val scope = CoroutineScope(StandardTestDispatcher())
        scope.launch(testHandler) {
            coEvery {
                repository.getList()
            } returns ResultModel.Success(listOf())
            getSatelliteListUseCase.executeAsync("").executeUseCase({
                assert(it.isEmpty())
            }, {
                assert(false)
            })
        }
        testHandler.cleanupTestCoroutines()
    }

    @Test
    fun `get list with filter from repository`() {
        val testHandler = TestCoroutineExceptionHandler()
        val scope = CoroutineScope(StandardTestDispatcher())
        scope.launch(testHandler) {
            coEvery {
                repository.getList()
            } returns ResultModel.Success(MockDataProvider.provideSingleItemList())
            getSatelliteListUseCase.executeAsync("first").executeUseCase({
                assert(MockDataProvider.provideSingleItemList().isNotEmpty())
            }, {
                assert(false)
            })
        }
        testHandler.cleanupTestCoroutines()
    }

    @Test
    fun `get list without filter from repository`() {
        val testHandler = TestCoroutineExceptionHandler()
        val scope = CoroutineScope(StandardTestDispatcher())
        scope.launch(testHandler) {
            coEvery {
                repository.getList()
            } returns ResultModel.Success(MockDataProvider.provideSatelliteList())
            getSatelliteListUseCase.executeAsync(null).executeUseCase({
                assert(MockDataProvider.provideSatelliteList().isNotEmpty())
            }, {
                assert(false)
            })
        }
        testHandler.cleanupTestCoroutines()
    }
}