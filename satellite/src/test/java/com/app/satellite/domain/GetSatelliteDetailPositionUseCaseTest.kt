package com.app.satellite.domain

import com.app.core.model.ResultModel
import com.app.core.useCase.executeUseCase
import com.app.satellite.MockDataProvider
import com.app.satellite.domain.useCase.GetSatelliteDetailPositionUseCase
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.launch
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.TestCoroutineExceptionHandler
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@ExperimentalCoroutinesApi
@RunWith(JUnit4::class)
class GetSatelliteDetailPositionUseCaseTest {

    private val repository: Repository = mockk()
    private val getSatelliteDetailPositionUseCase = GetSatelliteDetailPositionUseCase(repository)

    @Test
    fun `get detail position from repository`() {
        val testHandler = TestCoroutineExceptionHandler()
        val scope = CoroutineScope(StandardTestDispatcher())
        scope.launch(testHandler) {
            coEvery {
                repository.getPosition(1)
            } returns ResultModel.Success(listOf(MockDataProvider.provideSatellitePositionDetail()))
            getSatelliteDetailPositionUseCase.executeAsync(1).executeUseCase({
                assert(it.posX != 0.0)
            }, {
                assert(false)
            })
        }
        testHandler.cleanupTestCoroutines()
    }

    @Test
    fun `get detail empty position from repository`() {
        val testHandler = TestCoroutineExceptionHandler()
        val scope = CoroutineScope(StandardTestDispatcher())
        scope.launch(testHandler) {
            coEvery {
                repository.getPosition(1)
            } returns ResultModel.Success(listOf())
            getSatelliteDetailPositionUseCase.executeAsync(1).executeUseCase({
                assert(it.posX == 0.0)
            }, {
                assert(false)
            })
        }
        testHandler.cleanupTestCoroutines()
    }
}