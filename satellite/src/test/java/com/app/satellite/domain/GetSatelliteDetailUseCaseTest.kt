package com.app.satellite.domain

import com.app.core.model.ResultModel
import com.app.core.useCase.executeUseCase
import com.app.satellite.MockDataProvider
import com.app.satellite.domain.useCase.GetSatelliteDetailUseCase
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.launch
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.TestCoroutineExceptionHandler
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@ExperimentalCoroutinesApi
@RunWith(JUnit4::class)
class GetSatelliteDetailUseCaseTest {

    private val repository: Repository = mockk()
    private val getSatelliteDetailUseCase = GetSatelliteDetailUseCase(repository)

    @Test
    fun `get detail from repository`() {
        val testHandler = TestCoroutineExceptionHandler()
        val scope = CoroutineScope(StandardTestDispatcher())
        scope.launch(testHandler) {
            coEvery {
                repository.getDetail(1)
            } returns ResultModel.Success(MockDataProvider.provideSatelliteDetail())
            getSatelliteDetailUseCase.executeAsync(1).executeUseCase({
                assert(it.id == 1)
            }, {
                assert(false)
            })
        }
        testHandler.cleanupTestCoroutines()
    }

    @Test
    fun `get detail not found from repository`() {
        val testHandler = TestCoroutineExceptionHandler()
        val scope = CoroutineScope(StandardTestDispatcher())
        scope.launch(testHandler) {
            coEvery {
                repository.getDetail(2)
            } returns ResultModel.Success(MockDataProvider.provideSatelliteEmptyDetail())
            getSatelliteDetailUseCase.executeAsync(2).executeUseCase({
                assert(it.id == 0)
            }, {
                assert(false)
            })
        }
        testHandler.cleanupTestCoroutines()
    }

}