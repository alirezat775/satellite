package com.app.satellite.presentation.list

import androidx.lifecycle.MutableLiveData
import com.app.core.presentation.*
import com.app.core.useCase.executeUseCase
import com.app.satellite.domain.model.SatelliteModel
import com.app.satellite.domain.useCase.GetSatelliteListUseCase
import kotlinx.coroutines.delay

class SatelliteViewModel(
    private val getSatelliteListUseCase: GetSatelliteListUseCase,
) : BaseViewModel() {

    private val _listState = MutableLiveData<ViewState<List<SatelliteModel>>>()
    val listState = _listState

    fun getList(filterName: String?) {
        _listState.value = ViewLoading
        launch {
            getSatelliteListUseCase.executeAsync(filterName).executeUseCase({
                // delay just for appear loading state
                delay(3000)
                _listState.value = if (it.isEmpty()) {
                    ViewEmpty()
                } else {
                    ViewData(it)
                }
            }, {
                _listState.value = ViewError(it.message)
            })
        }
    }
}