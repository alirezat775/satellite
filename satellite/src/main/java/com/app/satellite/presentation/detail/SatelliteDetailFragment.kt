package com.app.satellite.presentation.detail

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import com.app.core.presentation.ViewData
import com.app.core.presentation.ViewError
import com.app.satellite.R
import com.app.satellite.databinding.FragmentSatelliteDetailBinding
import com.app.satellite.domain.model.SatelliteDetailModel
import com.app.satellite.domain.model.SatellitePositionModel
import com.google.android.material.snackbar.Snackbar
import org.koin.androidx.viewmodel.ext.android.viewModel

class SatelliteDetailFragment : Fragment(R.layout.fragment_satellite_detail) {

    private lateinit var binding: FragmentSatelliteDetailBinding
    private val viewModel by viewModel<SatelliteDetailViewModel>()
    private val args: SatelliteDetailFragmentArgs by navArgs()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.getDetail(args.id, args.name)
        viewModel.getPosition(args.id)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentSatelliteDetailBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeOnData()
    }

    private fun observeOnData() {
        viewModel.detailState.observe(viewLifecycleOwner) {
            when (it) {
                is ViewData -> {
                    setData(it.data)
                }
                is ViewError -> {
                    Snackbar.make(binding.root, it.message.toString(), Snackbar.LENGTH_LONG).show()
                }
            }
        }

        viewModel.detailPositionState.observe(viewLifecycleOwner) {
            when (it) {
                is ViewData -> {
                    setPositionData(it.data)
                }
                is ViewError -> {
                    Snackbar.make(binding.root, it.message.toString(), Snackbar.LENGTH_LONG).show()
                }
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private fun setPositionData(data: SatellitePositionModel) {
        binding.position.text =
            getString(R.string.last_position) + "(" + data.posX + ", " + data.posY + ")"
    }

    @SuppressLint("SetTextI18n")
    private fun setData(data: SatelliteDetailModel) {
        binding.name.text = data.name
        binding.date.text = data.firstFlight
        binding.height.text =
            getString(R.string.height_mass) + data.height +
                    getString(R.string.spliter) + data.mass
        binding.coast.text = getString(R.string.coast) + data.costPerLaunch
    }
}