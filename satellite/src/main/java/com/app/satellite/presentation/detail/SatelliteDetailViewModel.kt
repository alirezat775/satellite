package com.app.satellite.presentation.detail

import com.app.core.presentation.*
import com.app.core.useCase.executeUseCase
import com.app.satellite.domain.model.SatelliteDetailModel
import com.app.satellite.domain.model.SatellitePositionModel
import com.app.satellite.domain.useCase.GetSatelliteDetailPositionUseCase
import com.app.satellite.domain.useCase.GetSatelliteDetailUseCase
import kotlinx.coroutines.delay
import kotlinx.coroutines.isActive

class SatelliteDetailViewModel(
    private val getSatelliteDetailUseCase: GetSatelliteDetailUseCase,
    private val getSatelliteDetailPositionUseCase: GetSatelliteDetailPositionUseCase
) : BaseViewModel() {

    private val _detailState = SingleLiveEvent<ViewState<SatelliteDetailModel>>()
    val detailState = _detailState

    private val _detailPositionState = SingleLiveEvent<ViewState<SatellitePositionModel>>()
    val detailPositionState = _detailPositionState

    fun getDetail(id: Int, satelliteName: String) {
        launch {
            getSatelliteDetailUseCase.executeAsync(id).executeUseCase({
                val data = it.apply { name = satelliteName }
                _detailState.value = ViewData(data)
            }, {
                _detailState.value = ViewError(it.message)
            })
        }
    }

    fun getPosition(id: Int) {
        launch {
            while (isActive) {
                getSatelliteDetailPositionUseCase.executeAsync(id).executeUseCase({
                    _detailPositionState.value = ViewData(it)
                }, {
                    _detailPositionState.value = ViewError(it.message)
                })
                delay(3000)
            }
        }
    }
}