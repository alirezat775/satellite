package com.app.satellite.presentation.list

import android.graphics.Typeface
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import com.app.satellite.R
import com.app.satellite.databinding.ItemSatelliteBinding
import com.app.satellite.domain.model.SatelliteModel
import com.app.uikit.BaseRecyclerAdapter
import com.app.uikit.inflate

class SatelliteAdapter : BaseRecyclerAdapter() {

    class OnItemClick(val satellite: SatelliteModel)

    var actionClickListener: ((OnItemClick) -> Unit)? = null

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): BaseViewHolder<Any> {
        val holder =
            ItemViewHolder(ItemSatelliteBinding.bind(parent.inflate(R.layout.item_satellite)))
        holder.actionClickListener = actionClickListener
        return holder
    }

    class ItemViewHolder(private val binding: ItemSatelliteBinding) :
        BaseViewHolder<SatelliteModel>(binding.root) {

        var actionClickListener: ((OnItemClick) -> Unit)? = null

        override fun bind(data: SatelliteModel) {
            if (data.active) {
                binding.title.text = data.name
                binding.title.setTypeface(null, Typeface.BOLD)
                binding.status.text = binding.root.context.getString(R.string.active)
                binding.status.setTypeface(null, Typeface.BOLD)

                binding.image.setBackgroundColor(
                    ContextCompat.getColor(binding.root.context, R.color.green)
                )

                binding.title.setTextColor(
                    ContextCompat.getColor(binding.root.context, R.color.colorOnSurface)
                )
                binding.status.setTextColor(
                    ContextCompat.getColor(binding.root.context, R.color.colorOnSurface)
                )
            } else {
                binding.title.text = data.name
                binding.title.setTypeface(null, Typeface.NORMAL)
                binding.status.text = binding.root.context.getString(R.string.passive)
                binding.status.setTypeface(null, Typeface.NORMAL)

                binding.image.setBackgroundColor(
                    ContextCompat.getColor(binding.root.context, R.color.red)
                )

                binding.title.setTextColor(
                    ContextCompat.getColor(binding.root.context, R.color.gray)
                )
                binding.status.setTextColor(
                    ContextCompat.getColor(binding.root.context, R.color.gray)
                )
            }

            binding.root.setOnClickListener {
                actionClickListener?.invoke(OnItemClick(data))
            }
        }
    }
}