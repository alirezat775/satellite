package com.app.satellite.presentation.list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.core.presentation.ViewData
import com.app.core.presentation.ViewEmpty
import com.app.core.presentation.ViewError
import com.app.core.presentation.ViewLoading
import com.app.satellite.R
import com.app.satellite.databinding.FragmentSatelliteListBinding
import com.app.uikit.*
import com.google.android.material.snackbar.Snackbar
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.channels.consumeEach
import kotlinx.coroutines.launch
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.util.concurrent.TimeUnit


class SatelliteListFragment : Fragment(R.layout.fragment_satellite_list) {

    private lateinit var binding: FragmentSatelliteListBinding
    private val viewModel by viewModel<SatelliteViewModel>()
    private val satelliteAdapter: SatelliteAdapter by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.getList(null)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentSatelliteListBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupAdapter()
        observeOnData()
        setupSearch()
    }

    private fun setupSearch() {
        activity?.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN)
        MainScope().launch {
            binding.searchTextBox.onTextChanged()
                .debounce(this, 3, TimeUnit.SECONDS)
                .consumeEach {
                    viewModel.getList(it)
                }
        }
    }

    private fun observeOnData() {
        viewModel.listState.observe(viewLifecycleOwner) {
            when (it) {
                is ViewLoading -> {
                    binding.loading.visible()
                    binding.empty.gone()
                    binding.recyclerView.gone()
                }
                is ViewData -> {
                    binding.loading.gone()
                    binding.empty.gone()
                    binding.recyclerView.visible()
                    satelliteAdapter.setItems(it.data)
                }
                is ViewError -> {
                    binding.empty.visible()
                    binding.loading.gone()
                    binding.recyclerView.gone()
                    Snackbar.make(binding.root, it.message.toString(), Snackbar.LENGTH_LONG).show()
                }
                is ViewEmpty -> {
                    binding.empty.visible()
                    binding.loading.gone()
                    binding.recyclerView.gone()
                }
            }
        }
    }

    private fun setupAdapter() {
        binding.recyclerView.apply {
            adapter = satelliteAdapter
            layoutManager =
                LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
            addItemDecoration(DividerItemDecorator(context))
        }

        satelliteAdapter.actionClickListener = {
            findNavController().navigate(
                SatelliteListFragmentDirections.openDetail(it.satellite.id, it.satellite.name)
            )
        }
    }
}