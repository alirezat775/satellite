package com.app.satellite.domain.useCase

import com.app.core.model.ResultModel
import com.app.core.model.map
import com.app.core.useCase.AsyncUseCase
import com.app.satellite.domain.Repository
import com.app.satellite.domain.model.SatellitePositionModel

class GetSatelliteDetailPositionUseCase(
    private val repository: Repository
) : AsyncUseCase<Int, ResultModel<SatellitePositionModel>> {

    override suspend fun executeAsync(rq: Int): ResultModel<SatellitePositionModel> {
        val list = repository.getPosition(rq)
        return list.map { it.random() }
    }
}
