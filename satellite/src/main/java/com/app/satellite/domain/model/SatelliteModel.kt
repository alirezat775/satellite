package com.app.satellite.domain.model

data class SatelliteModel(
    val id: Int,
    val active: Boolean,
    val name: String
)
