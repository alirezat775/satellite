package com.app.satellite.domain.model

data class SatellitePositionModel(
    val posX: Double,
    val posY: Double
)