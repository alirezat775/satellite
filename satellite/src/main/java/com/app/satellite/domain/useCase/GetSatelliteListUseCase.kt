package com.app.satellite.domain.useCase

import com.app.core.model.ResultModel
import com.app.core.model.map
import com.app.core.useCase.AsyncUseCase
import com.app.satellite.domain.Repository
import com.app.satellite.domain.model.SatelliteModel

class GetSatelliteListUseCase(
    private val repository: Repository
) : AsyncUseCase<String?, ResultModel<List<SatelliteModel>>> {

    override suspend fun executeAsync(rq: String?): ResultModel<List<SatelliteModel>> {
        val repoList = repository.getList()
        return if (rq.isNullOrEmpty()) {
            repoList
        } else {
            repoList.map { list ->
                list.filter { it.name.uppercase().contains(rq.uppercase()) }
            }
        }
    }
}