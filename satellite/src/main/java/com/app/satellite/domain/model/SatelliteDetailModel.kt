package com.app.satellite.domain.model

data class SatelliteDetailModel(
    val id: Int,
    var name: String? = null,
    val costPerLaunch: Long,
    val firstFlight: String,
    val height: Int,
    val mass: Int
)