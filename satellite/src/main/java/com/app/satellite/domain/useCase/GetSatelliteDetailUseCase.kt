package com.app.satellite.domain.useCase

import com.app.core.model.ResultModel
import com.app.core.useCase.AsyncUseCase
import com.app.satellite.domain.Repository
import com.app.satellite.domain.model.SatelliteDetailModel

class GetSatelliteDetailUseCase(
    private val repository: Repository
) : AsyncUseCase<Int, ResultModel<SatelliteDetailModel>> {

    override suspend fun executeAsync(rq: Int): ResultModel<SatelliteDetailModel> {
        return repository.getDetail(rq)
    }
}