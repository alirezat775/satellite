package com.app.satellite.domain

import com.app.core.model.ResultModel
import com.app.satellite.domain.model.SatelliteDetailModel
import com.app.satellite.domain.model.SatelliteModel
import com.app.satellite.domain.model.SatellitePositionModel

interface Repository {

    suspend fun getList(): ResultModel<List<SatelliteModel>>

    suspend fun getDetail(id: Int): ResultModel<SatelliteDetailModel>

    suspend fun getPosition(id: Int): ResultModel<List<SatellitePositionModel>>
}