package com.app.satellite.di

import com.app.satellite.presentation.detail.SatelliteDetailViewModel
import com.app.satellite.presentation.list.SatelliteAdapter
import com.app.satellite.presentation.list.SatelliteViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val satellitePresentationModule = module {
    viewModel { SatelliteViewModel(get()) }
    viewModel { SatelliteDetailViewModel(get(), get()) }
    factory { SatelliteAdapter() }
}