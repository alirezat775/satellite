package com.app.satellite.di

import android.content.Context
import androidx.room.Room
import com.app.core.helper.FileHelper
import com.app.satellite.data.Mapper
import com.app.satellite.data.RepositoryImpl
import com.app.satellite.data.cacheDataSource.CacheDataSource
import com.app.satellite.data.cacheDataSource.CacheDataSourceImpl
import com.app.satellite.data.cacheDataSource.CacheDatabase
import com.app.satellite.data.cacheDataSource.SatelliteDao
import com.app.satellite.data.localDataSource.LocalDataSource
import com.app.satellite.data.localDataSource.LocalDataSourceImpl
import com.app.satellite.domain.Repository
import org.koin.dsl.module

val satelliteDataModule = module {
    factory<LocalDataSource> { LocalDataSourceImpl(get()) }
    factory<CacheDataSource> { CacheDataSourceImpl(provideSatelliteDao(get())) }
    factory<Repository> { RepositoryImpl(get(), get(), get()) }
    factory { Mapper() }
    factory { FileHelper(get()) }
}

private fun provideSatelliteDao(context: Context): SatelliteDao {
    return Room.databaseBuilder(context, CacheDatabase::class.java, "database-satellite").build()
        .satellite()
}