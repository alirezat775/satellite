package com.app.satellite.di

object SatelliteInjector {

    fun provideDependencies() = listOf(
        satelliteDataModule,
        satelliteDomainModule,
        satellitePresentationModule
    )
}