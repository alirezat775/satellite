package com.app.satellite.di

import com.app.satellite.domain.useCase.GetSatelliteDetailPositionUseCase
import com.app.satellite.domain.useCase.GetSatelliteDetailUseCase
import com.app.satellite.domain.useCase.GetSatelliteListUseCase
import org.koin.dsl.module

val satelliteDomainModule = module {
    factory { GetSatelliteListUseCase(get()) }
    factory { GetSatelliteDetailUseCase(get()) }
    factory { GetSatelliteDetailPositionUseCase(get()) }
}
