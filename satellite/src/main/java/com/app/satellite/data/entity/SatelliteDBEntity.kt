package com.app.satellite.data.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "satellite")
data class SatelliteDBEntity(
    @PrimaryKey(autoGenerate = true)
    val id: Int,
    @ColumnInfo(name = "cost_per_launch")
    val cost_per_launch: Long,
    @ColumnInfo(name = "first_flight")
    val first_flight: String,
    @ColumnInfo(name = "height")
    val height: Int,
    @ColumnInfo(name = "mass")
    val mass: Int
)
