package com.app.satellite.data.entity

data class SatellitePositionListEntity(
    val list: List<SatellitePositionEntity>
)

data class SatellitePositionEntity(
    val id: Int,
    val positions: List<SatellitePositionItemEntity>
)

data class SatellitePositionItemEntity(
    val posX: Double,
    val posY: Double
)
