package com.app.satellite.data.localDataSource

import com.app.core.model.ResultModel
import com.app.satellite.data.entity.SatelliteDetailEntity
import com.app.satellite.data.entity.SatelliteEntity
import com.app.satellite.data.entity.SatellitePositionEntity
import com.app.satellite.data.entity.SatellitePositionListEntity

interface LocalDataSource {

    suspend fun getList(): ResultModel<List<SatelliteEntity>>

    suspend fun getDetail(id: Int): ResultModel<SatelliteDetailEntity>

    suspend fun getPosition(id: Int): ResultModel<SatellitePositionEntity>

}