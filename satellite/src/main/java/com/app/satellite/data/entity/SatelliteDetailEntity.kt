package com.app.satellite.data.entity

data class SatelliteDetailEntity(
    val id: Int,
    val cost_per_launch: Long,
    val first_flight: String,
    val height: Int,
    val mass: Int
)