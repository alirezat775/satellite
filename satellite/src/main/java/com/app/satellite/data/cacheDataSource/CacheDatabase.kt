package com.app.satellite.data.cacheDataSource

import androidx.room.Database
import androidx.room.RoomDatabase
import com.app.satellite.data.entity.SatelliteDBEntity

@Database(entities = [SatelliteDBEntity::class], version = 1)
abstract class CacheDatabase : RoomDatabase() {
    abstract fun satellite(): SatelliteDao
}
