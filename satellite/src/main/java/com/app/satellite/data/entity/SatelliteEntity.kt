package com.app.satellite.data.entity

data class SatelliteEntity(
    val id: Int,
    val active: Boolean,
    val name: String
)
