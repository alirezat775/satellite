package com.app.satellite.data.cacheDataSource

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.app.satellite.data.entity.SatelliteDBEntity

@Dao
interface SatelliteDao {

    @Insert
    @Throws(Exception::class)
    suspend fun add(entity: SatelliteDBEntity)

    @Query("select * from satellite where id=:id")
    @Throws(Exception::class)
    suspend fun get(id: Int): SatelliteDBEntity?
}
