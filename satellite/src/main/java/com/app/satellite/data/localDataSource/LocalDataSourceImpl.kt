package com.app.satellite.data.localDataSource

import android.content.Context
import com.app.core.helper.FileHelper
import com.app.core.model.ErrorModel
import com.app.core.model.ResultModel
import com.app.satellite.data.entity.SatelliteDetailEntity
import com.app.satellite.data.entity.SatelliteEntity
import com.app.satellite.data.entity.SatellitePositionEntity
import com.app.satellite.data.entity.SatellitePositionListEntity
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class LocalDataSourceImpl(
    private val fileHelper: FileHelper,
) : LocalDataSource {

    override suspend fun getList(): ResultModel<List<SatelliteEntity>> {
        return withContext(Dispatchers.IO) {
            try {
                val gson: Gson = GsonBuilder().create()
                val json = fileHelper.loadJSONFromAsset("satellite-list.json")
                val satelliteList = gson.fromJson(json, Array<SatelliteEntity>::class.java)
                ResultModel.Success(satelliteList.toMutableList())
            } catch (e: Exception) {
                ResultModel.Error(ErrorModel(e.message, e))
            }
        }
    }

    override suspend fun getDetail(id: Int): ResultModel<SatelliteDetailEntity> {
        return withContext(Dispatchers.IO) {
            try {
                val gson: Gson = GsonBuilder().create()
                val json = fileHelper.loadJSONFromAsset("satellite-detail.json")
                val satelliteList = gson.fromJson(json, Array<SatelliteDetailEntity>::class.java)
                val satellite = satelliteList.first { it.id == id }
                ResultModel.Success(satellite)
            } catch (e: Exception) {
                ResultModel.Error(ErrorModel(e.message, e))
            }
        }
    }

    override suspend fun getPosition(id: Int): ResultModel<SatellitePositionEntity> {
        return withContext(Dispatchers.IO) {
            try {
                val gson: Gson = GsonBuilder().create()
                val json = fileHelper.loadJSONFromAsset("satellite-positions.json")
                val satellitePositionList =
                    gson.fromJson(json, SatellitePositionListEntity::class.java)
                val item = satellitePositionList.list.first { it.id == id }
                ResultModel.Success(item)
            } catch (e: Exception) {
                ResultModel.Error(ErrorModel(e.message, e))
            }
        }
    }
}