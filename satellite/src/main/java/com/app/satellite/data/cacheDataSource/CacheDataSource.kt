package com.app.satellite.data.cacheDataSource

import com.app.core.model.ResultModel
import com.app.satellite.data.entity.SatelliteDBEntity

interface CacheDataSource {

    suspend fun getDetail(id: Int): ResultModel<SatelliteDBEntity?>

    suspend fun addDetail(item: SatelliteDBEntity): ResultModel<Unit>

}