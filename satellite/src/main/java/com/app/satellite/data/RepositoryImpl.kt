package com.app.satellite.data

import com.app.core.model.ResultModel
import com.app.core.model.get
import com.app.core.model.getOrNull
import com.app.core.model.map
import com.app.satellite.data.cacheDataSource.CacheDataSource
import com.app.satellite.data.localDataSource.LocalDataSource
import com.app.satellite.domain.Repository
import com.app.satellite.domain.model.SatelliteDetailModel
import com.app.satellite.domain.model.SatelliteModel
import com.app.satellite.domain.model.SatellitePositionModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class RepositoryImpl(
    private val localDataSource: LocalDataSource,
    private val cacheDataSource: CacheDataSource,
    private val mapper: Mapper
) : Repository {

    override suspend fun getList(): ResultModel<List<SatelliteModel>> {
        return localDataSource.getList().map {
            it.map { item -> mapper.satelliteEntityToModel(item) }
        }
    }

    override suspend fun getDetail(id: Int): ResultModel<SatelliteDetailModel> {
        val cacheItem = cacheDataSource.getDetail(id)
        return if (cacheItem.getOrNull() != null) {
            cacheItem.map { mapper.satelliteDBEntityToModel(cacheItem.get()) }
        } else {
            val localData = localDataSource.getDetail(id)
            withContext(Dispatchers.IO) {
                cacheDataSource.addDetail(mapper.satelliteEntityToDBEntity(localData.getOrNull()))
            }
            localData.map { mapper.satelliteDetailEntityToModel(it) }
        }
    }

    override suspend fun getPosition(id: Int): ResultModel<List<SatellitePositionModel>> {
        return localDataSource.getPosition(id).map { list ->
            list.positions.map { item -> mapper.satellitePositionEntityToModel(item) }
        }
    }

}