package com.app.satellite.data

import com.app.satellite.data.entity.SatelliteDBEntity
import com.app.satellite.data.entity.SatelliteDetailEntity
import com.app.satellite.data.entity.SatelliteEntity
import com.app.satellite.data.entity.SatellitePositionItemEntity
import com.app.satellite.domain.model.SatelliteDetailModel
import com.app.satellite.domain.model.SatelliteModel
import com.app.satellite.domain.model.SatellitePositionModel

class Mapper {

    fun satelliteEntityToModel(it: SatelliteEntity): SatelliteModel {
        return SatelliteModel(it.id, it.active, it.name)
    }

    fun satelliteDetailEntityToModel(it: SatelliteDetailEntity): SatelliteDetailModel {
        return SatelliteDetailModel(
            id = it.id,
            costPerLaunch = it.cost_per_launch,
            firstFlight = it.first_flight,
            height = it.height,
            mass = it.mass
        )
    }

    fun satellitePositionEntityToModel(it: SatellitePositionItemEntity): SatellitePositionModel {
        return SatellitePositionModel(it.posX, it.posY)
    }

    fun satelliteEntityToDBEntity(it: SatelliteDetailEntity?): SatelliteDBEntity {
        return SatelliteDBEntity(
            id = it?.id ?: 0,
            cost_per_launch = it?.cost_per_launch ?: 0,
            first_flight = it?.first_flight ?: "",
            height = it?.height ?: 0,
            mass = it?.mass ?: 0
        )
    }

    fun satelliteDBEntityToModel(it: SatelliteDBEntity?): SatelliteDetailModel {
        return SatelliteDetailModel(
            id = it?.id ?: 0,
            costPerLaunch = it?.cost_per_launch ?: 0,
            firstFlight = it?.first_flight ?: "",
            height = it?.height ?: 0,
            mass = it?.mass ?: 0
        )
    }

}