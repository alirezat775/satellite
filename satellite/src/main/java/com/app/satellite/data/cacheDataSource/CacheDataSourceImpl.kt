package com.app.satellite.data.cacheDataSource

import com.app.core.model.ErrorModel
import com.app.core.model.ResultModel
import com.app.satellite.data.entity.SatelliteDBEntity

class CacheDataSourceImpl(
    private val dao: SatelliteDao
) : CacheDataSource {

    override suspend fun getDetail(id: Int): ResultModel<SatelliteDBEntity?> {
        return try {
            ResultModel.Success(dao.get(id))
        } catch (e: Exception) {
            ResultModel.Error(ErrorModel(e.message, e))
        }
    }

    override suspend fun addDetail(item: SatelliteDBEntity): ResultModel<Unit> {
        return try {
            ResultModel.Success(dao.add(item))
        } catch (e: Exception) {
            ResultModel.Error(ErrorModel(e.message, e))
        }
    }

}